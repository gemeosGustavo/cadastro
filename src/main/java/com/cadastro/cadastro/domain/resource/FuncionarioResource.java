package com.cadastro.cadastro.domain.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cadastro.cadastro.domain.model.Funcionario;
import com.cadastro.cadastro.domain.repository.FuncionarioRepository;

@RestController
@RequestMapping(value = "/funcionario")
public class FuncionarioResource {

	@Autowired
	private FuncionarioRepository funcionarioRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Funcionario> addFuncionario(@RequestBody Funcionario funcionario) {

		Funcionario newFuncionario = funcionarioRepository.save(funcionario);

		return ResponseEntity.status(HttpStatus.CREATED).body(newFuncionario);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Funcionario>> getFuncionarios() {

		List<Funcionario> lstFuncionario = funcionarioRepository.findAll();

		if (lstFuncionario.isEmpty()) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Funcionario>>(lstFuncionario, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Funcionario> getFuncionario(@PathVariable("id") long id) {

		Funcionario capFuncionario = funcionarioRepository.findOne(id);

		if (capFuncionario == null) {
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Funcionario>(capFuncionario, HttpStatus.OK);
	}

}
