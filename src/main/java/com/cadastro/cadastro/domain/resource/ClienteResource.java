package com.cadastro.cadastro.domain.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cadastro.cadastro.domain.model.Cliente;
import com.cadastro.cadastro.domain.repository.ClienteRepository;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteResource {
	

	@Autowired
	private ClienteRepository clienteRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Cliente> addCliente(@RequestBody Cliente cliente){
		
		Cliente newCliente = clienteRepository.save(cliente);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(newCliente);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Cliente>> getClientes(){	
		
		List<Cliente> lstCliente = clienteRepository.findAll();
		
		if(lstCliente.isEmpty()) {
			return new ResponseEntity<List<Cliente>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Cliente>>(lstCliente , HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}") 
	public ResponseEntity<Cliente> getCliente(@PathVariable("id") long id){
		
		Cliente capCliente = clienteRepository.findOne(id);
		
		if(capCliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(capCliente, HttpStatus.OK);
	}

}
